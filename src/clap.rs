//! Command line argument parsing module.

use clap_lib::{ Arg, App } ;

static log_key: & 'static str = "LOG" ;
static success_key: & 'static str = "SUCCESS" ;
static seed_key: & 'static str = "SEED" ;
static verbose_key: & 'static str = "VERB" ;

static log_file: & 'static str = "log.smt2" ;

#[derive(Debug)]
pub struct Conf {
  pub log: Option<& 'static str>,
  pub print_success: bool,
  pub seed: Option<String>,
  pub verbose: bool,
}

pub fn work() -> Conf {
  // Handling CLA (Command Line Arguments).
  let matches = {
    App::new("texdown to ...").version(
      "0.1.0"
    ).author(
      "Adrien Champion <adrien.champion@email.com>"
    ).about(
      "SMT-based AI for 2048"
    ).arg(
      Arg::with_name(seed_key).short("s").long("seed").help(
        "Sets the seed for 2048"
      ).takes_value(true)
    ).arg(
      Arg::with_name(log_key).short("l").long("log").help(
        "Logs interactions with the SMT solver"
      )
    ).arg(
      Arg::with_name(success_key).short("p").long("print_success").help(
        "Solver will be launched in print success mode"
      )
    ).arg(
      Arg::with_name(verbose_key).short("v").help("Activates verbose output")
    ).get_matches()
  } ;

  Conf {
    log: match matches.occurrences_of(log_key) {
      0 => None,
      _ => Some(log_file),
    },
    seed: match matches.value_of(seed_key) {
      Some(seed) => Some(seed.to_string()),
      None => None,
    },
    print_success: matches.occurrences_of(success_key) > 0,
    verbose: matches.occurrences_of(verbose_key) > 0,
  }
}
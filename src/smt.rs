//! Code dealing with the `rsmt2` library.

use std::io::Write ;

use rsmt2::{ Sort2Smt, Sym2Smt, Expr2Smt, ParseSmt2, IoResUnit } ;
use nom::IResult ;

/// Sorts (types). Used to tell the SMT solver what the signature of a symbol
/// is.
pub enum Sort {
  /// Bool sort.
  Bool,
  /// Int sort.
  Int,
}
impl Sort2Smt for Sort {
  fn sort_to_smt2(& self, writer: & mut Write) -> IoResUnit {
    write!(
      writer, "{}", match * self {
        Sort::Bool => "bool",
        Sort::Int => "Int",
      }
    )
  }
}

/// A symbol is a string.
#[derive(Debug, Clone, PartialEq)]
pub struct Sym {
  /// Actual symbol.
  sym: String
}
impl Sym {
  #[inline(always)]
  pub fn mk(sym: String) -> Self {
    Sym { sym: sym }
  }
  #[inline(always)]
  pub fn of(sym: & 'static str) -> Self {
    Sym { sym: sym.to_string() }
  }
  #[inline(always)]
  pub fn get(& self) -> & str { & self.sym }
}
impl Sym2Smt<()> for Sym {
  fn sym_to_smt2(& self, writer: & mut Write, _: & ()) -> IoResUnit {
    write!(writer, "{}", self.sym)
  }
}

/// Boolean and integer values.
#[derive(Debug, Clone, PartialEq)]
pub enum Val {
  /// A boolean.
  B(bool),
  /// An integer.
  I(usize),
}

/// An arithmetic expression.
#[derive(Debug, Clone, PartialEq)]
pub enum AExpr {
  /// N-ary addition.
  Add(Vec<AExpr>),
  /// N-ary difference.
  Sub(Vec<AExpr>),
  /// Int constant.
  Val(usize),
  /// Int variable.
  Var(Sym),
}
impl AExpr {
  #[inline(always)]
  pub fn val(n: usize) -> Self { AExpr::Val(n) }
  #[inline(always)]
  pub fn var(s: Sym) -> Self { AExpr::Var(s) }
}

impl Expr2Smt<()> for AExpr {
  fn expr_to_smt2(& self, writer: & mut Write, none: & ()) -> IoResUnit {
    // Contains recursive calls, but expressions should be too shallow to
    // overflow.
    match * self {
      AExpr::Add(ref kids) => {
        try!( write!(writer, "(+") ) ;
        for kid in kids {
          try!( write!(writer, " ") ) ;
          try!( kid.expr_to_smt2(writer, none) )
        } ;
        write!(writer, ")")
      },
      AExpr::Sub(ref kids) => {
        try!( write!(writer, "(-") ) ;
        for kid in kids {
          try!( write!(writer, " ") ) ;
          try!( kid.expr_to_smt2(writer, none) )
        } ;
        write!(writer, ")")
      },
      AExpr::Val(n) => write!(writer, "{}", n),
      AExpr::Var(ref sym) => write!(writer, "{}", sym.get()),
    }
  }
}

/// Constructors for arithmetic expressions. (Unused atm, see
/// [`expr!`](../macro.expr!.html)).
pub trait AExprOps<Rhs> {
  /// Creates a `<`.
  #[inline(always)]
  fn lt(self, Rhs) -> BExpr ;
  /// Creates a `<=`.
  #[inline(always)]
  fn le(self, Rhs) -> BExpr ;
  /// Creates a `>=`.
  #[inline(always)]
  fn ge(self, Rhs) -> BExpr ;
  /// Creates a `>`.
  #[inline(always)]
  fn gt(self, Rhs) -> BExpr ;
  /// Creates a `=`.
  #[inline(always)]
  fn eq(self, Rhs) -> BExpr ;
  /// Creates a sum.
  #[inline(always)]
  fn add(self, Rhs) -> AExpr ;
  /// Creates a difference.
  #[inline(always)]
  fn sub(self, Rhs) -> AExpr ;
}

/// Wrapper around any expression.
#[derive(Debug, Clone, PartialEq)]
pub enum WExpr {
  B(BExpr),
  A(AExpr),
}

/// Formulas.
#[derive(Debug, Clone, PartialEq)]
pub enum BExpr {
  /// N-ary conjunction.
  Conj(Vec<BExpr>),
  /// N-ary disjunction.
  Disj(Vec<BExpr>),
  /// Implication.
  Impl(Box<BExpr>, Box<BExpr>),
  /// If-then-else.
  Ite(Box<BExpr>, Box<BExpr>, Box<BExpr>),
  /// Less than.
  Lt(AExpr, AExpr),
  /// Less than or equal to.
  Le(AExpr, AExpr),
  /// Greater than or equal to.
  Ge(AExpr, AExpr),
  /// Greater than.
  Gt(AExpr, AExpr),
  /// Equal (on arithmetic).
  Eq(AExpr, AExpr),
  /// Boolean constant.
  Val(bool),
  /// Boolean variable.
  Var(Sym),
  /// Function application.
  App(Sym, Vec<WExpr>),
}
impl BExpr {
  /// Creates a constant.
  pub fn val(b: bool) -> Self { BExpr::Val(b) }
  /// Creates a variable.
  pub fn var(v: Sym) -> Self { BExpr::Var(v) }
  /// Creates a conjunction.
  pub fn and(mut elms: Vec<BExpr>) -> Self {
    match elms.len() {
      0 => BExpr::val(false),
      1 => elms.pop().unwrap(),
      _ => BExpr::Conj(elms)
    }
  }
  /// Creates a disjunction.
  pub fn or(mut elms: Vec<BExpr>) -> Self {
    match elms.len() {
      0 => BExpr::val(true),
      1 => elms.pop().unwrap(),
      _ => BExpr::Disj(elms)
    }
  }
  /// Creates an implication.
  pub fn implies(lhs: Self, rhs: Self) -> Self {
    BExpr::Impl(Box::new(lhs), Box::new(rhs))
  }
  /// Creates an if-then-else.
  pub fn ite(cond: Self, thn: Self, els: Self) -> Self {
    BExpr::Ite(Box::new(cond), Box::new(thn), Box::new(els))
  }
  /// Creates a `<`.
  pub fn lt(lhs: AExpr, rhs: AExpr) -> Self {
    BExpr::Lt(lhs, rhs)
  }
  /// Creates a `<=`.
  pub fn le(lhs: AExpr, rhs: AExpr) -> Self {
    BExpr::Le(lhs, rhs)
  }
  /// Creates a `>=`.
  pub fn ge(lhs: AExpr, rhs: AExpr) -> Self {
    BExpr::Ge(lhs, rhs)
  }
  /// Creates a `>=`.
  pub fn gt(lhs: AExpr, rhs: AExpr) -> Self {
    BExpr::Gt(lhs, rhs)
  }
}

impl Expr2Smt<()> for BExpr {
  fn expr_to_smt2(& self, writer: & mut Write, none: & ()) -> IoResUnit {
    // Contains recursive calls, but expressions should be too shallow to
    // overflow.
    match * self {
      BExpr::Conj(ref kids) => {
        try!( write!(writer, "(and") ) ;
        for kid in kids {
          try!( write!(writer, " ") ) ;
          try!( kid.expr_to_smt2(writer, none) )
        } ;
        write!(writer, ")")
      },
      BExpr::Disj(ref kids) => {
        try!( write!(writer, "(or") ) ;
        for kid in kids {
          try!( write!(writer, " ") ) ;
          try!( kid.expr_to_smt2(writer, none) )
        } ;
        write!(writer, ")")
      },
      BExpr::Impl(ref lhs, ref rhs) => {
        try!( write!(writer, "(=> ") ) ;
        try!( (* lhs).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* rhs).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Ite(ref cond, ref thn, ref els) => {
        try!( write!(writer, "(ite ") ) ;
        try!( (* cond).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* thn).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* els).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Lt(ref lhs, ref rhs) => {
        try!( write!(writer, "(< ") ) ;
        try!( (* lhs).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* rhs).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Le(ref lhs, ref rhs) => {
        try!( write!(writer, "(<= ") ) ;
        try!( (* lhs).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* rhs).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Ge(ref lhs, ref rhs) => {
        try!( write!(writer, "(>= ") ) ;
        try!( (* lhs).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* rhs).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Gt(ref lhs, ref rhs) => {
        try!( write!(writer, "(> ") ) ;
        try!( (* lhs).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* rhs).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Eq(ref lhs, ref rhs) => {
        try!( write!(writer, "(< ") ) ;
        try!( (* lhs).expr_to_smt2(writer, none) ) ;
        try!( write!(writer, " ") ) ;
        try!( (* rhs).expr_to_smt2(writer, none) ) ;
        write!(writer, ")")
      },
      BExpr::Val(b) => write!(writer, "{}", b),
      BExpr::Var(ref sym) => write!(writer, "{}", sym.get()),
      BExpr::App(ref sym, ref args) => {
        try!( write!(writer, "(") ) ;
        try!( sym.sym_to_smt2(writer, none) ) ;
        for arg in args {
          try!( write!(writer, " ") ) ;
          match * arg {
            WExpr::B(ref b_expr) => try!( b_expr.expr_to_smt2(writer, none) ),
            WExpr::A(ref a_expr) => try!( a_expr.expr_to_smt2(writer, none) ),
          }
        }
        write!(writer, ")")
      },
    }
  }
}


/// Constructors for boolean expressions. (Unused atm, see
/// [`expr!`](../macro.expr!.html)).
pub trait BExprOps<Rhs> {
  /// Creates a conjunction.
  #[inline(always)]
  fn and(self, Rhs) -> BExpr ;
  /// Creates a disjunction.
  #[inline(always)]
  fn or(self, Rhs) -> BExpr ;
}






/// Parser.
pub struct Parser ;
impl ParseSmt2 for Parser {
  type Ident = Sym ;
  type Value = Val ;
  type Expr = BExpr ;
  type Proof = () ;
  type I = () ;

  fn parse_ident<'a>(
    & self, _: & 'a [u8]
  ) -> IResult<& 'a [u8], Sym> {
    panic!("not implemented")
  }
  fn parse_value<'a>(
    & self, _: & 'a [u8]
  ) -> IResult<& 'a [u8], Val> {
    panic!("not implemented")
  }
  fn parse_expr<'a>(
    & self, _: & 'a [u8], _: & ()
  ) -> IResult<& 'a [u8], BExpr> {
    panic!("not implemented")
  }
  fn parse_proof<'a>(
    & self, _: & 'a [u8]
  ) -> IResult<& 'a [u8], ()> {
    panic!("not implemented")
  }
}












/// Expression DSL.
///
/// Can construct bool and arith expressions.
#[macro_export]
macro_rules! expr {
  (ite $c:expr, $t:expr, $e:expr) => (
    $crate::smt::BExpr::Ite(
      Box::new($c), Box::new($t), Box::new($e)
    )
  ) ;
  (=> $lhs:expr, $rhs:expr) => (
    $crate::smt::BExpr::Impl(
      Box::new($lhs), Box::new($rhs)
    )
  ) ;
  (and $( $elm:expr ),+ ) => (
    $crate::smt::BExpr::Conj( vec![ $( $elm ),+ ] )
  ) ;
  (or $( $elm:expr ),+ ) => (
    $crate::smt::BExpr::Disj( vec![$($elm),+] )
  ) ;

  (+ $( $elm:expr ),+ ) => (
    $crate::smt::AExpr::Add( vec![$($elm),+] )
  ) ;
  (- $( $elm:expr ),+ ) => (
    $crate::smt::AExpr::Sub( vec![$($elm),+] )
  ) ;
  (< $lhs:expr, $rhs:expr) => (
    $crate::smt::BExpr::Lt( $lhs, $rhs )
  ) ;
  (<= $lhs:expr, $rhs:expr) => (
    $crate::smt::BExpr::Le( $lhs, $rhs )
  ) ;
  (>= $lhs:expr, $rhs:expr) => (
    $crate::smt::BExpr::Ge( $lhs, $rhs )
  ) ;
  (> $lhs:expr, $rhs:expr) => (
    $crate::smt::BExpr::Gt( $lhs, $rhs )
  ) ;
  (= $lhs:expr, $rhs:expr) => (
    $crate::smt::AExpr::Eq(
      Box::new($lhs), Box::new($rhs)
    )
  ) ;
  (true) => ($crate::smt::BExpr::val(true)) ;
  (false) => ($crate::smt::BExpr::val(false)) ;
  (bool $e:expr) => ($crate::smt::BExpr::var($e)) ;
  (int $e:expr) => ($crate::smt::AExpr::var($e)) ;
}

















impl AExprOps<AExpr> for AExpr {
  fn lt(self, rhs: AExpr) -> BExpr {
    BExpr::Lt(self, rhs)
  }
  fn le(self, rhs: AExpr) -> BExpr {
    BExpr::Le(self, rhs)
  }
  fn ge(self, rhs: AExpr) -> BExpr {
    BExpr::Ge(self, rhs)
  }
  fn gt(self, rhs: AExpr) -> BExpr {
    BExpr::Gt(self, rhs)
  }
  fn eq(self, rhs: AExpr) -> BExpr {
    BExpr::Eq(self, rhs)
  }
  fn add(self, rhs: AExpr) -> AExpr {
    AExpr::Add( vec![self, rhs] )
  }
  fn sub(self, rhs: AExpr) -> AExpr {
    AExpr::Sub( vec![self, rhs] )
  }
}




impl<'a> AExprOps<AExpr> for & 'a AExpr {
  fn lt(self, rhs: AExpr) -> BExpr {
    BExpr::Lt(self.clone(), rhs)
  }
  fn le(self, rhs: AExpr) -> BExpr {
    BExpr::Le(self.clone(), rhs)
  }
  fn ge(self, rhs: AExpr) -> BExpr {
    BExpr::Ge(self.clone(), rhs)
  }
  fn gt(self, rhs: AExpr) -> BExpr {
    BExpr::Gt(self.clone(), rhs)
  }
  fn eq(self, rhs: AExpr) -> BExpr {
    BExpr::Eq(self.clone(), rhs)
  }
  fn add(self, rhs: AExpr) -> AExpr {
    AExpr::Add( vec![self.clone(), rhs] )
  }
  fn sub(self, rhs: AExpr) -> AExpr {
    AExpr::Sub( vec![self.clone(), rhs] )
  }
}






impl<'b> AExprOps<& 'b AExpr> for AExpr {
  fn lt(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Lt(self, rhs.clone())
  }
  fn le(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Le(self, rhs.clone())
  }
  fn ge(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Ge(self, rhs.clone())
  }
  fn gt(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Gt(self, rhs.clone())
  }
  fn eq(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Eq(self, rhs.clone())
  }
  fn add(self, rhs: & 'b AExpr) -> AExpr {
    AExpr::Add( vec![self, rhs.clone()] )
  }
  fn sub(self, rhs: & 'b AExpr) -> AExpr {
    AExpr::Sub( vec![self, rhs.clone()] )
  }
}





impl<'a, 'b> AExprOps<& 'b AExpr> for & 'a AExpr {
  fn lt(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Lt(self.clone(), rhs.clone())
  }
  fn le(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Le(self.clone(), rhs.clone())
  }
  fn ge(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Ge(self.clone(), rhs.clone())
  }
  fn gt(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Gt(self.clone(), rhs.clone())
  }
  fn eq(self, rhs: & 'b AExpr) -> BExpr {
    BExpr::Eq(self.clone(), rhs.clone())
  }
  fn add(self, rhs: & 'b AExpr) -> AExpr {
    AExpr::Add( vec![self.clone(), rhs.clone()] )
  }
  fn sub(self, rhs: & 'b AExpr) -> AExpr {
    AExpr::Sub( vec![self.clone(), rhs.clone()] )
  }
}





impl BExprOps<BExpr> for BExpr {
  fn and(self, rhs: BExpr) -> BExpr {
    BExpr::Conj(vec![self, rhs] )
  }
  fn or(self, rhs: BExpr) -> BExpr {
    BExpr::Disj( vec![self, rhs] )
  }
}
impl BExprOps<Vec<BExpr>> for BExpr {
  fn and(self, mut rhs: Vec<BExpr>) -> BExpr {
    rhs.push(self) ; // Order doesn't matter.
    BExpr::Conj( rhs )
  }
  fn or(self, mut rhs: Vec<BExpr>) -> BExpr {
    rhs.push(self) ; // Order doesn't matter.
    BExpr::Disj( rhs )
  }
}
impl<'a> BExprOps<BExpr> for & 'a BExpr {
  fn and(self, rhs: BExpr) -> BExpr {
    BExpr::Conj( vec![self.clone(), rhs] )
  }
  fn or(self, rhs: BExpr) -> BExpr {
    BExpr::Disj( vec![self.clone(), rhs] )
  }
}
impl<'a> BExprOps<Vec<BExpr>> for & 'a BExpr {
  fn and(self, mut rhs: Vec<BExpr>) -> BExpr {
    rhs.push(self.clone()) ; // Order doesn't matter.
    BExpr::Conj( rhs )
  }
  fn or(self, mut rhs: Vec<BExpr>) -> BExpr {
    rhs.push(self.clone()) ; // Order doesn't matter.
    BExpr::Disj( rhs )
  }
}
//! Skeleton crate for a 2048 AI.
#![allow(unused_imports, non_upper_case_globals)]

extern crate clap as clap_lib ;
extern crate rsmt2 ;
extern crate lib_2048 ;
extern crate nom ;

use lib_2048::Evolution ;
use lib_2048::rendering::{ Frame, rendering_loop } ;

use rsmt2::{ Solver, Query } ;

/// Gathers the useful solver traits under a single trait.
pub trait SolverT<'kid> : Solver<'kid, Parser> + Query<'kid, Parser> {}
impl<'kid> SolverT<'kid> for rsmt2::PlainSolver<'kid, Parser> {}
impl<'kid> SolverT<'kid> for rsmt2::TeeSolver<'kid, Parser> {}

pub mod clap ;
#[macro_use]
pub mod smt ;
pub mod constraints ;

use clap::Conf as ClapConf ;

use smt::Parser ;


/** Convenience macro. */
macro_rules! smtry {
  ($e:expr, failwith $( $msg:expr ),+) => (
    match $e {
      Ok(something) => something,
      Err(e) => panic!( $($msg),+ , e)
    }
  ) ;
}

/// Sleeps for some time (in ms).
#[allow(dead_code)]
fn sleep(ms: u64) {
  use std::thread::sleep ;
  use std::time::Duration ;
  sleep(Duration::from_millis(ms)) ;
}

/// Dumb AI that tries to go up, then left, then right, then down.
#[allow(dead_code)]
fn ai_move(frame: & mut Frame) -> Evolution {
  use std::process::exit ;
  sleep(20) ;
  match frame.up() {
    Evolution::Nothing => match frame.left() {
      Evolution::Nothing => match frame.right() {
        Evolution::Nothing => match frame.down() {
          Evolution::Nothing => {
            println!("I lost T_T") ;
            println!("") ;
            exit(0)
          },
          evol => evol,
        },
        evol => evol,
      },
      evol => evol,
    },
    evol => evol,
  }
}

fn main() {
  use std::process::exit ;

  let conf = clap::work() ;

  launch(& conf) ;

  // rendering_loop(seed, painter, ai_move)
}



fn launch(clap_conf: & ClapConf) {
  use rsmt2::* ;
  use std::fs::OpenOptions ;

  let seed = match clap_conf.seed {
    None => lib_2048::Seed::mk(),
    Some(ref seed) => lib_2048::Seed::of_str(seed),
  } ;

  let painter = lib_2048::rendering::Painter::default() ;

  let conf = SolverConf::z3() ;
  let conf = if clap_conf.print_success {
    conf.print_success()
  } else { conf } ;

  let mut kid = smtry!(
    Kid::mk(conf),
    failwith "Could not spawn solver kid: {:?}"
  ) ;

  {

    let mut solver = smtry!(
      solver(& mut kid, smt::Parser),
      failwith "could not create solver: {:?}"
    ) ;

    match clap_conf.log {
      None => work(& mut solver),
      Some(file) => {
        let file = smtry!(
          OpenOptions::new()
          .create(true).write(true).truncate(true).open(file),
          failwith "could not open \"log.smt2\": {:?}"
        ) ;
        work(& mut solver.tee(file))
      },
    }
  }

  smtry!(
    kid.kill(),
    failwith "error killing solver: {:?}"
  )
}


/// Type parameters to abstract between a plain solver and a tee solver.
pub fn work<'kid, S: SolverT<'kid>>(solver: & mut S) {
  use smt::* ;

  let sym_x = Sym::of("x") ;
  let sym_y = Sym::of("y") ;

  smtry!(
    solver.declare_fun(& sym_x, & [], & Sort::Int, & ()),
    failwith "declaration for x failed: {:?}"
  ) ;
  smtry!(
    solver.declare_fun(& sym_y, & [], & Sort::Int, & ()),
    failwith "declaration for y failed: {:?}"
  ) ;

  let expr_x = expr!(int sym_x.clone()) ;
  let expr_y = expr!(int sym_y.clone()) ;

  let a_expr = expr!(
    + expr_x, expr!(
      - expr_y, AExpr::val(7)
    )
  ) ;

  let b_expr = expr!(> a_expr, AExpr::val(13)) ;
  let b_expr = expr!(
    ite BExpr::val(false), BExpr::val(false),
    expr!(=> BExpr::val(true), b_expr)
  ) ;

  smtry!(
    solver.assert(& b_expr, & ()),
    failwith "assert failed: {:?}"
  ) ;

  match smtry!(
    solver.check_sat(), failwith "error in checksat: {:?}"
  ) {
    true => println!("sat"),
    false => println!("unsat"),
  } ;

  let sym_z = Sym::of("z") ;
  let sym_fun = Sym::of("fun") ;



  smtry!(
    solver.define_fun(
      & sym_fun,
      & [ (sym_z.clone(), Sort::Int) ],
      & Sort::Bool,
      & expr!(
        < expr!(int sym_z), AExpr::val(7)
      ),
      & ()
    ),
    failwith "could not define fun: {:?}"
  ) ;

  smtry!(
    solver.assert(
      & BExpr::App( sym_fun, vec![ WExpr::A(expr!(int sym_x.clone())) ] ),
      & ()
    ),
    failwith "could not assert: {:?}"
  )
}